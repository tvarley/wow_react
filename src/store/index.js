import { createStore, applyMiddleware } from "redux";
import asyncDispatchMiddleware from "store/asyncDispatchMiddleware";
import reducer from "reducers/index";

const store = createStore(reducer, applyMiddleware(asyncDispatchMiddleware));

export default store;
