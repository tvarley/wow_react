import { MATERIALS } from "constants/materials";
import { mockAuctions } from "mock/auction_mock";

import {
  fetchAuctionsSuccess,
  fetchAuctionsError,
  fetchAuctionsProcess
} from "actions";

import {
  FETCH_AUCTIONS,
  FETCH_AUCTIONS_STATUS_FETCHING,
  FETCH_AUCTIONS_STATUS_SUCCESS,
  FETCH_AUCTIONS_STATUS_ERROR,
  FETCH_AUCTIONS_STATUS_PROCESSING
} from "constants/action-types";

const blizzard = require("blizzard.js").initialize({
  key: process.env.REACT_APP_BLIZZARD_CLIENT_ID,
  secret: process.env.REACT_APP_BLIZZARD_CLIENT_SECRET
});

const auctionsInitialState = {
  materials: []
};

export default function auctions(state = auctionsInitialState, action) {
  switch (action.type) {
    case FETCH_AUCTIONS: {
      switch (action.status) {
        case FETCH_AUCTIONS_STATUS_FETCHING: {
          blizzard.getApplicationToken().then(response => {
            blizzard.defaults.token = response.data.access_token;
            const { realmSlug } = action.payload;
            blizzard.wow.auction({ realm: realmSlug }).then(r2 => {
              if (r2.status === 200) {
                const { files } = r2.data;
                action.asyncDispatch(
                  fetchAuctionsSuccess({ url: files[0].url })
                );
              } else {
                action.asyncDispatch(
                  fetchAuctionsError("Failed to get realm list")
                );
              }
            });
          });
          return Object.assign({}, state, {
            isFetching: true,
            statusMessage: "Fetching Auctions"
          });
        }
        case FETCH_AUCTIONS_STATUS_SUCCESS: {
          action.asyncDispatch(fetchAuctionsProcess(mockAuctions));
          // TODO: This is going to need a/ Proxy server and a caching strategy
          // const dataUrl = action.payload.url;
          // const axios = require("axios");
          // const jsonpAdapter = require('axios-jsonp');
          // axios.get(dataUrl,{
          // adapter: jsonpAdapter,
          // headers: {
          //   'Content-Type': 'application/json;charset=utf-8',
          //   accept: 'application/json'
          // },
          // data: {}})
          // .then( res => res.json() )
          // .then( json => console.log(json));
          // fetch(dataUrl, { headers: {
          // Accept: 'application/json'
          // }, mode: 'no-cors'})
          // .then( res => res.json() )
          // .then( json => console.log(json));
          // fetchJsonp(dataUrl, { timeout: 240000 }).then( function(response) {
          // fetch(dataUrl, { mode: 'no-cors'}).then( function(response) {
          // debugger;
          // return response.json;
          // }).then(function(json) {
          // debugger;
          // action.asyncDispatch(fetchAuctionsProcess(json));
          // }).catch(function(ex) {
          // action.asyncDispatch(
          //   fetchAuctionsError("Failed to parse auction data")
          // );
          // });
          return Object.assign({}, state, {
            isFetching: true,
            statusMessage: "Fetching auction data dump"
          });
        }
        case FETCH_AUCTIONS_STATUS_PROCESSING: {
          const { auctionList } = action.payload;
          const accum = MATERIALS;
          const totals = {};
          auctionList
            .filter(auction => {
              if (auction.buyout === 0) {
                return false;
              }
              return true;
            })
            .forEach(auction => {
              if (totals[auction.item] === undefined) {
                totals[auction.item] = {
                  total: 0,
                  quantity: 0,
                  min: Number.POSITIVE_INFINITY
                };
              }
              totals[auction.item].min = Math.min(
                totals[auction.item].min,
                auction.buyout / auction.quantity
              );
              totals[auction.item].total += auction.buyout;
              totals[auction.item].quantity += auction.quantity;
            });
          const results = [];
          accum
            .filter(material => {
              if (material.not_auction === undefined) {
                return true;
              }
              return false;
            })
            .forEach(material => {
              results.push({
                name: material.name,
                number: material.number,
                price: totals[material.number].min,
                quantity: totals[material.number].quantity
              });
            });
          return Object.assign({}, state, {
            isFetching: false,
            materials: results,
            statusMessage: "Auctions processed"
          });
        }
        case FETCH_AUCTIONS_STATUS_ERROR: {
          return Object.assign({}, state, {
            isFetching: false,
            statusMessage: action.message
          });
        }
        default: {
          action.asyncDispatch(
            fetchAuctionsError("Invalid auction fetch state")
          );
          break;
        }
      }
      break;
    }
    default: {
      return state;
    }
  }
  return state;
}
