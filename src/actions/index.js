import {
  FETCH_REALM_LIST,
  FETCH_REALM_STATUS_FETCHING,
  FETCH_REALM_STATUS_ERROR,
  FETCH_REALM_STATUS_SUCCESS,
  SELECT_REALM,
  FETCH_AUCTIONS,
  FETCH_AUCTIONS_STATUS_FETCHING,
  FETCH_AUCTIONS_STATUS_ERROR,
  FETCH_AUCTIONS_STATUS_SUCCESS,
  FETCH_AUCTIONS_STATUS_PROCESSING
} from "constants/action-types";

export function fetchAuctions(payload) {
  return {
    type: FETCH_AUCTIONS,
    status: FETCH_AUCTIONS_STATUS_FETCHING,
    payload
  };
}

export function fetchAuctionsSuccess(payload) {
  return {
    type: FETCH_AUCTIONS,
    status: FETCH_AUCTIONS_STATUS_SUCCESS,
    payload
  };
}

export function fetchAuctionsProcess(payload) {
  return {
    type: FETCH_AUCTIONS,
    status: FETCH_AUCTIONS_STATUS_PROCESSING,
    payload
  };
}

export function fetchAuctionsError(message) {
  return {
    type: FETCH_AUCTIONS,
    status: FETCH_AUCTIONS_STATUS_ERROR,
    message
  };
}

export function fetchRealmList() {
  return { type: FETCH_REALM_LIST, status: FETCH_REALM_STATUS_FETCHING };
}

export function fetchRealmListError(message) {
  return {
    type: FETCH_REALM_LIST,
    status: FETCH_REALM_STATUS_ERROR,
    message
  };
}

export function fetchRealmListSuccess(payload) {
  return {
    type: FETCH_REALM_LIST,
    status: FETCH_REALM_STATUS_SUCCESS,
    payload
  };
}

export function selectRealm(payload) {
  return { type: SELECT_REALM, payload };
}
