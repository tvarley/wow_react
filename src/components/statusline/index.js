import React, { Component } from "react";
import { connect } from "react-redux";

class StatusLine extends Component {
  render() {
    const { currentRealm, statusMessage, isFetching } = this.props;
    let spinner = "";
    if (isFetching) {
      spinner = (
        <div
          className="spinner-border spinner-border-sm mr-1"
          role="status"
          aria-hidden="true"
        />
      );
    }
    return (
      <div className="mr-4">
        {spinner}
        {statusMessage} |<strong> Current Realm: {currentRealm}</strong>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { currentRealm, statusMessage, isFetching } = state.realmList;
  return { currentRealm, statusMessage, isFetching };
}

export default connect(mapStateToProps)(StatusLine);
