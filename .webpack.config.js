const webpack = require('webpack');

module.exports = (env) => {
  // this object is our actual webpack config
  return {
    plugins: [
      // add the plugin to your plugins array
      new webpack.DefinePlugin({ 
        `process.env.REACT_APP_TEST_VAR`: JSON.stringify(${env.REACT_APP_TEST_VAR}) 
      })
    ]
  };
};
