export const FETCH_REALM_LIST = "FETCH_REALM_LIST";
export const FETCH_REALM_STATUS_FETCHING = "fetching";
export const FETCH_REALM_STATUS_ERROR = "error";
export const FETCH_REALM_STATUS_SUCCESS = "success";

export const FETCH_AUCTIONS = "FETCH_AUCTIONS";
export const FETCH_AUCTIONS_STATUS_FETCHING = "fetching";
export const FETCH_AUCTIONS_STATUS_ERROR = "error";
export const FETCH_AUCTIONS_STATUS_SUCCESS = "success";
export const FETCH_AUCTIONS_STATUS_PROCESSING = "processing";

export const SELECT_REALM = "SELECT_REALM";
