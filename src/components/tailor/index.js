import React, { Component } from "react";

import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import ListGroup from "react-bootstrap/ListGroup";
import MaterialsTable from "components/materialstable";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import { connect } from "react-redux";

function formatPrice(price) {
  if (price === 0.0) {
    return "--";
  }
  return `${(price / 10000).toFixed(2)}g`;
}

function extractPrice(materials, number, quantity) {
  if (materials.length === 0) {
    return 0.0;
  }
  if (number === 159959) {
    return 5200;
  }
  if (number === 152668) {
    return 0;
  }
  const mat = materials.find(element => {
    return element.number === number;
  });
  return mat.price * quantity;
}

function recipe(materials, ingredients, quantity) {
  let total = 0.0;
  ingredients.forEach(ingredient => {
    total += extractPrice(materials, ingredient.number, ingredient.quantity);
  });
  return total * quantity;
}

function totalCost(materials) {
  if (materials.length === 0) {
    return 0.0;
  }
  // Tidespray Linen = 10 * 100
  // Nylon Thread = (5 * 100) + (8 * 13)
  // Deep Sea Satin = (22 * 13)
  return (
    extractPrice(materials, 152576, 1000) +
    extractPrice(materials, 159959, 604) +
    extractPrice(materials, 152577, 286)
  );
}

function totalSellable(materials) {
  if (materials.length === 0) {
    return 0.0;
  }
  // Umbra Shard 34
  // Veiled Crystal 16
  // Gloom Dust 31
  // Tidespray Linen 126
  return (
    extractPrice(materials, 152876, 34) +
    extractPrice(materials, 152877, 16) +
    extractPrice(materials, 152875, 31) +
    extractPrice(materials, 152576, 126)
  );
}

class Tailor extends Component {
  render() {
    const { materials } = this.props;
    return (
      <Container>
        <Row>
          <Col>
            <h2>Tailor</h2>
            <Card>
              <ListGroup>
                <ListGroup.Item>
                  Total Cost: {formatPrice(totalCost(materials))}
                </ListGroup.Item>
                <ListGroup.Item>
                  Total Sellable: {formatPrice(totalSellable(materials))}
                </ListGroup.Item>
                <ListGroup.Item>
                  Total Profit:{" "}
                  {formatPrice(totalSellable(materials) - totalCost(materials))}
                </ListGroup.Item>
              </ListGroup>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <MaterialsTable />
          </Col>
        </Row>
        <Row>
          <Col>
            <h4>Stage 1 Bracers</h4>
            <Table striped bordered size="sm">
              <thead>
                <tr>
                  <th>Bracer</th>
                  <th>Recipe</th>
                  <th>Cost</th>
                  <th>Per 100</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Tidespray Bracer</td>
                  <td>10 Tidespray Linen, 5 Nylon Thread </td>
                  <td>
                    {formatPrice(
                      recipe(
                        materials,
                        [
                          { number: 152576, quantity: 10 },
                          { number: 159959, quantity: 5 }
                        ],
                        1
                      )
                    )}
                  </td>
                  <td>
                    {formatPrice(
                      recipe(
                        materials,
                        [
                          { number: 152576, quantity: 10 },
                          { number: 159959, quantity: 5 }
                        ],
                        100
                      )
                    )}
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col>
            <p>
              Disenchant Blues, Scrap Green Bracers from Step 1 above. Use
              Expulsom in Stage 2
            </p>
            <p>Expected Expulsom (BOP) from Stage 1 = 13</p>
            <h4>Stage 2 Bracers</h4>
            <Table striped bordered size="sm">
              <thead>
                <tr>
                  <th>Bracer</th>
                  <th>Recipe</th>
                  <th>Cost</th>
                  <th>Per 13</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Honorable Cloth Bracer - Rank 3</td>
                  <td>22 Deep Sea Satin, 8 Nylon Thread, 1 Expulsom</td>
                  <td>
                    {formatPrice(
                      recipe(
                        materials,
                        [
                          { number: 152577, quantity: 22 },
                          { number: 159959, quantity: 8 }
                        ],
                        1
                      )
                    )}
                  </td>
                  <td>
                    {formatPrice(
                      recipe(
                        materials,
                        [
                          { number: 152577, quantity: 22 },
                          { number: 159959, quantity: 8 }
                        ],
                        13
                      )
                    )}
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <Row>
          <Col>
            <p>
              Disenchant Bracers from Stage 2. Sell all materials. Make Enchants
            </p>
            <h4>Sell Materials</h4>
            <Table striped bordered size="sm">
              <thead>
                <tr>
                  <th>Material</th>
                  <th>Expected return</th>
                  <th>Price each</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Umbra Shard</td>
                  <td>34</td>
                  <td>{formatPrice(extractPrice(materials, 152876, 1))}</td>
                  <td>{formatPrice(extractPrice(materials, 152876, 33))}</td>
                </tr>
                <tr>
                  <td>Veiled Crystal</td>
                  <td>16</td>
                  <td>{formatPrice(extractPrice(materials, 152877, 1))}</td>
                  <td>{formatPrice(extractPrice(materials, 152877, 16))}</td>
                </tr>
                <tr>
                  <td>Gloom Dust</td>
                  <td>31</td>
                  <td>{formatPrice(extractPrice(materials, 152875, 1))}</td>
                  <td>{formatPrice(extractPrice(materials, 152875, 31))}</td>
                </tr>
                <tr>
                  <td>Tidespray Linen</td>
                  <td>126</td>
                  <td>{formatPrice(extractPrice(materials, 152576, 1))}</td>
                  <td>{formatPrice(extractPrice(materials, 152576, 126))}</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  const { materials } = state.auctions;
  return { materials };
}

export default connect(mapStateToProps)(Tailor);
