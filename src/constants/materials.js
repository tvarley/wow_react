export const MATERIALS = [
  { name: "Blood Stained Bone", number: 154164, price: 0.0, quantity: 0 },
  { name: "Calcified Bone", number: 154165, price: 0.0, quantity: 0 },
  { name: "Coarse Leather", number: 152541, price: 0.0, quantity: 0 },
  { name: "Deep Sea Satin", number: 152577, price: 0.0, quantity: 0 },
  {
    name: "Expulsom",
    number: 152668,
    price: 0.0,
    quantity: 0,
    not_auction: true
  },
  { name: "Gloom Dust", number: 152875, price: 0.0, quantity: 0 },
  { name: "Mistscale", number: 153051, price: 0.0, quantity: 0 },
  {
    name: "Nylon Thread",
    number: 159959,
    price: 0.52,
    quantity: 0,
    not_auction: true
  },
  { name: "Shimmerscale", number: 153050, price: 0.0, quantity: 0 },
  { name: "Tempest Hide", number: 154722, price: 0.0, quantity: 0 },
  { name: "Tidespray Linen", number: 152576, price: 0.0, quantity: 0 },
  { name: "Umbra Shards", number: 152876, price: 0.0, quantity: 0 },
  { name: "Veiled Crystal", number: 152877, price: 0.0, quantity: 0 }
];

export default MATERIALS;
