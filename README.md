## Profession Shuffling. 

Hosted at Heroku: https://wow-shuffle.herokuapp.com/
It is hosted on the hobby tier so it maybe asleep, poke it a couple of times and it will wake up.

A shuffle is the process of taking one or more materials, crafting a specific item with them, then doing something else with the crafted item. The goal is to use crafting to increase the value of the raw materials.

This application is based on the Multi stage shuffle for Battle for Azeroth first described by the WoW Goblin Samadan.

See: https://twitter.com/samadanplayswow/status/1096047728080101376?lang=en


