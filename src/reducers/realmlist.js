import React from "react";

import {
  fetchRealmListSuccess,
  fetchRealmListError,
  fetchAuctions
} from "actions";

import {
  FETCH_REALM_LIST,
  FETCH_REALM_STATUS_FETCHING,
  FETCH_REALM_STATUS_ERROR,
  FETCH_REALM_STATUS_SUCCESS,
  SELECT_REALM
} from "constants/action-types";

const blizzard = require("blizzard.js").initialize({
  key: process.env.REACT_APP_BLIZZARD_CLIENT_ID,
  secret: process.env.REACT_APP_BLIZZARD_CLIENT_SECRET
});

const realmlistInitialState = {
  realmList: [],
  statusMessage: "Initializing",
  currentRealm: "-- NONE --",
  currentRealmSlug: ""
};

export default function realmList(state = realmlistInitialState, action) {
  switch (action.type) {
    case FETCH_REALM_LIST: {
      switch (action.status) {
        case FETCH_REALM_STATUS_FETCHING: {
          if (state.realmList.length === 0) {
            blizzard.getApplicationToken().then(response => {
              blizzard.defaults.token = response.data.access_token;
              blizzard.wow.realm().then(r2 => {
                if (r2.status === 200) {
                  const { realms } = r2.data;
                  action.asyncDispatch(fetchRealmListSuccess(realms));
                } else {
                  action.asyncDispatch(
                    fetchRealmListError("Failed to get realm list")
                  );
                }
              });
            });
          }
          return Object.assign({}, state, {
            isFetching: true,
            statusMessage: "Fetching Realms"
          });
        }
        case FETCH_REALM_STATUS_ERROR: {
          return Object.assign({}, state, {
            isFetching: false,
            statusMessage: action.message
          });
        }
        case FETCH_REALM_STATUS_SUCCESS: {
          const options = [];
          const sortedRealms = action.payload.sort(function nameSort(a, b) {
            const ax = a.name.toLowerCase();
            const bx = b.name.toLowerCase();
            if (ax < bx) {
              return -1;
            }
            if (ax > bx) {
              return 1;
            }
            return 0;
          });
          for (let i = 0; i < sortedRealms.length; i += 1) {
            const realm = sortedRealms[i];
            options.push(
              <option key={realm.slug} value={realm.slug}>
                {realm.name}
              </option>
            );
          }
          return Object.assign({}, state, {
            isFetching: false,
            realmList: options,
            statusMessage: "Realms Loaded"
          });
        }
        default: {
          action.asyncDispatch(
            fetchRealmListError("Invalid realm fetch state")
          );
          break;
        }
      }
      break;
    }
    case SELECT_REALM: {
      const { realmName, realmSlug } = action.payload;
      action.asyncDispatch(fetchAuctions({ realmSlug }));
      return Object.assign({}, state, {
        currentRealm: realmName,
        currentRealmSlug: realmSlug
      });
    }
    default: {
      return state;
    }
  }
  return state;
}
