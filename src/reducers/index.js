import { combineReducers } from "redux";
import realmList from "reducers/realmlist";
import auctions from "reducers/auctions";

export default combineReducers({
  realmList,
  auctions
});
