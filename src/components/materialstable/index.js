import React, { Component } from "react";

import Table from "react-bootstrap/Table";
import { connect } from "react-redux";

export function formatPrice(price) {
  return `${(price / 10000).toFixed(2)}g`;
}

class MaterialsTable extends Component {
  render() {
    const { materials } = this.props;
    return (
      <Table striped bordered size="sm">
        <thead>
          <tr>
            <th>Material</th>
            <th>Price</th>
            <th>Available</th>
          </tr>
        </thead>
        <tbody>
          {materials
            .filter(mat => {
              if (mat.number === 152668 || mat.number === 159959) {
                return false;
              }
              return true;
            })
            .map(mat => (
              <tr key={mat.number}>
                <td>
                  <a href={`https://www.wowhead.com/item=${mat.number}`}>
                    {mat.name}
                  </a>
                </td>
                <td>{formatPrice(mat.price)}</td>
                <td>{mat.quantity}</td>
              </tr>
            ))}
        </tbody>
      </Table>
    );
  }
}

function mapStateToProps(state) {
  const { materials } = state.auctions;
  return { materials };
}

export default connect(mapStateToProps)(MaterialsTable);
