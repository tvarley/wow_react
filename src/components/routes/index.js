import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

import Home from "components/home";
import Leatherworker from "components/leatherworker";
import RealmList from "components/realmlist";
import StatusLine from "components/statusline";
import Tailor from "components/tailor";

class Routes extends Component {
  render() {
    return (
      <Router>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">WoW Shuffle</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto" activeKey="/">
              <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/tailor">Tailor</Nav.Link>
              <Nav.Link href="/leatherworker">Leatherworker</Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <div className="d-flex align-items-center">
            <StatusLine />
          </div>
          <Form className="form-inline my-2 my-lg-0">
            <RealmList />
          </Form>
        </Navbar>
        <div className="container">
          <Route exact path="/" component={Home} />
          <Route path="/tailor" component={Tailor} />
          <Route path="/leatherworker" component={Leatherworker} />
        </div>
      </Router>
    );
  }
}

export default Routes;
