import React, { Component } from "react";

import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { connect } from "react-redux";
import {
  fetchRealmList as fetchRealmListAction,
  selectRealm as selectRealmAction
} from "actions";

class RealmList extends Component {
  componentDidMount() {
    const { fetchRealmList } = this.props;
    fetchRealmList();
  }

  changeRealm(event) {
    const { selectRealm } = this.props;
    selectRealm({
      realmSlug: event.target.value,
      realmName: event.target.options[event.target.selectedIndex].text
    });
  }

  render() {
    const { realmList } = this.props;
    return (
      <InputGroup>
        <InputGroup.Prepend>
          <InputGroup.Text id="realm-select">Realm</InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          className="input-sm"
          onChange={this.changeRealm.bind(this)}
          as="select"
          id="realm-select-control"
        >
          <option value="no-select">Select</option>
          {realmList}
        </Form.Control>
      </InputGroup>
    );
  }
}

function mapStateToProps(state) {
  const { realmList } = state.realmList;
  return { realmList };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchRealmList: () => dispatch(fetchRealmListAction({})),
    selectRealm: payload => dispatch(selectRealmAction(payload))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RealmList);
