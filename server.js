// server.js
const express = require("express");
const favicon = require("express-favicon");
const path = require("path");

const port = process.env.PORT || 8080;
const app = express();

app.use(favicon(`${__dirname}/build/favicon-96x96.png`));
// the __dirname is the current directory from where the script is running
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "build")));

app.get("/ping", function pinger(req, res) {
  return res.send("pong");
});

app.get("/*", function reactor(req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(port);
