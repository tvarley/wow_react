import React, { Component } from "react";

import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import MaterialsTable from "components/materialstable";

import diagram from "assets/bracer_shuffle.png";

class Home extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col>
            <div className="text-center">
              <img src={diagram} alt="Diagram" />
            </div>
            <ol>
              <li>Select your realm above to begin</li>
              <li>Select your profession above to use the calculators</li>
            </ol>
            <MaterialsTable />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;
