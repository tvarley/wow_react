import React from "react";

import { Provider } from "react-redux";
import Routes from "./components/routes";
import store from "./store/index";

import "./App.css";

require("es6-promise").polyfill();

export default () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);
